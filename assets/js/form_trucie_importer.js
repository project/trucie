/**
 * @file
 * Behavior for Trucie Importer form.
 */

(function (Drupal, drupalSettings) {

  /**
   * Brings some magic to the trucie importer form.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.formTrucieImporter = {

    /**
     * {@inheritDoc}
     */
    attach(context) {
      this.initEntityTypeSelect(context);
      this.initFieldFieldsets(context);
      this.initFieldProcessorFieldsets(context);
    },

    /**
     * Inits entity type select widget.
     */
    initEntityTypeSelect(context) {
      const _self = this;

      once('form-trucie-importer', '#edit-import-entity-type', context).forEach(($select) => {
        const changeEvent = new Event('change');
        const $bundleSelect = document.querySelector('#edit-import-entity-bundle');
        const value = $bundleSelect.value;

        $select.addEventListener('change', _self.changeEntityType);
        $select.dispatchEvent(changeEvent);
        $bundleSelect.value = value;
      })
    },

    /**
     * Handles the entity type change.
     *
     * @param {object} e
     *   The change event.
     */
    changeEntityType(e) {
      const $typeSelect = e.target;
      const type = $typeSelect.value;
      const $bundleSelect = document.querySelector('#edit-import-entity-bundle');
      const bundles = drupalSettings.trucieImporter.entityTypes[type] ? drupalSettings.trucieImporter.entityTypes[type].bundles : [];
      let options = [];

      for (let i in bundles) {
        options.push('<option value="' + i + '">' + bundles[i] + '</option>');
      }

      $bundleSelect.innerHTML = options.join("\n");
    },

    /**
     * Adds useful stuff to field fieldsets.
     */
    initFieldFieldsets(context) {
      const _self = this;

      once('form-trucie-importer', '.fieldset-field', context).forEach(($fieldset) => {
        _self.addDeleteButton($fieldset)
        _self.initAddFieldProcessorButton($fieldset);
      });

      this.initAddFieldButton(context);
    },

    /**
     * Finds 'Add field' button and make it work.
     */
    initAddFieldButton(context) {
      const _self = this;

      once('form-trucie-importer', '.edit-add-field', context).forEach(($button) => {
        $button.addEventListener('click', _self.addField);

        let $container = document.querySelector('.field-container');

        // Add a first item (if not already).
        $container.dataset.children = $container.querySelectorAll('.fieldset-field').length;
        if (parseInt($container.dataset.children) < 2) {
          $button.click();
        }
      })
    },

    /**
     * Adds useful stuff to field processor fieldsets.
     */
    initFieldProcessorFieldsets(context) {
      const _self = this;

      once('form-trucie-importer', '.fieldset-field-processor', context).forEach(($fieldset) => {
        _self.addDeleteButton($fieldset);
        _self.initProcessorSelect($fieldset);
      });
    },

    /**
     * Finds 'Add processor' button and make it work.
     */
    initAddFieldProcessorButton(context) {
      const _self = this;

      once('form-trucie-importer', '.edit-add-processor', context).forEach(($button) => {
        $button.addEventListener('click', _self.addFieldProcessor);

        let $container = $button
          .closest('.fieldset')
          .querySelector('.field-processor-container');

        // Add a first item (if not already).
        $container.dataset.children = $container.querySelectorAll('.fieldset-field-processor:not(.hidden)').length;
        if (parseInt($container.dataset.children) < 1) {
          $button.click();
        }

        _self.initProcessorSelect($container);
      });
    },

    /**
     * Inits processor select widget and makes it work.
     */
    initProcessorSelect(context) {
      const _self = this;

      once('form-trucie-importer', '.form-select', context).forEach(($select) => {
        const changeEvent = new Event('change');

        $select.addEventListener('change', _self.changeProcessor);
        $select.dispatchEvent(changeEvent);
      })
    },

    /**
     * Handles the processor change.
     *
     * @param {object} e
     *   The change event.
     */
    changeProcessor(e) {
      const $select = e.target;
      const $container = $select.closest('.fieldset');
      const $params = $container.querySelectorAll('.processor-param');
      const type = $select.value;
      const typeData = drupalSettings.trucieImporter.processorTypes[type];

      $params.forEach(($field) => {
        const $container = $field.closest('.form-item');
        const param = $field.dataset.paramName;

        if (typeData && typeData.params && typeData.params[param]) {
          $container.querySelector('label').innerHTML = typeData.params[param].title;
          $container.style.display = 'block';

          if (typeData.params[param].required) {
            $field.setAttribute('required', 'required');
          }
        }
        else {
          $field.removeAttribute('required');
          $container.style.display = 'none';
        }
      });
    },

    /**
     * Adds new field fieldset to the form.
     *
     * @param {object} e
     *   The click event.
     */
    addField(e) {
      const _self = Drupal.behaviors.formTrucieImporter;
      const $container = document.querySelector('.field-container');
      const $template = document.querySelector('.field-template');
      let num = parseInt($container.dataset.children ? $container.dataset.children : 1);
      let cssClass = 'field-' + num;
      let child = $template.outerHTML;

      child = child
        .replace(/\s*data-once="form-trucie-importer"/g, '')
        .replace(/\[0]/g, '[' + num + ']')
        .replace(/-0-/g, '-' + num + '-')
        .replace(/data-num="0"/g, 'data-num="' + num + '"')
        .replace('hidden ', '')
        .replace('field-template', cssClass);

      $container.insertAdjacentHTML('beforeend', child);
      $container.dataset.children = num + 1;

      _self.initAddFieldProcessorButton($container.querySelector('.' + cssClass));
      e.preventDefault();
    },

    /**
     * Adds new field processor fieldset to the form.
     *
     * @param {object} e
     *   The click event.
     */
    addFieldProcessor(e) {
      const _self = Drupal.behaviors.formTrucieImporter;
      const $parent = e.target.closest('.fieldset')
      const $container = $parent.querySelector('.field-processor-container');
      const $template = document.querySelector('.field-processor-template');
      let parentNum = parseInt($parent.dataset.num ? $parent.dataset.num : 1);
      let num = parseInt($container.dataset.children ? $container.dataset.children : 0) + 1;
      let cssClass = 'field-processor-' + num;
      let child = $template.outerHTML;

      child = child
        .replace(/\s*data-once="form-trucie-importer"/g, '')
        .replace(/00:/g, num + ':')
        .replace(/0:/g, num + ':')
        .replace(/\[00]/g, '[' + num + ']')
        .replace(/-00-/g, '-' + num + '-')
        .replace(/-00/g, '-' + parentNum + '-' + num)
        .replace(/\[0]/g, '[' + parentNum + ']')
        .replace(/-0-/g, '-' + parentNum + '-')
        .replace('hidden ', '')
        .replace('field-processor-template', cssClass);

      $container.insertAdjacentHTML('beforeend', child);
      $container.dataset.children = num;

      _self.initProcessorSelect($container.querySelector('.' + cssClass));
      e.preventDefault();
    },

    /**
     * Adds delete button to the fieldset.
     *
     * @param {object} $fieldset
     *   The fieldset element.
     */
    addDeleteButton($fieldset) {
      let $legend = $fieldset.querySelector('legend span');

      if (!$legend) {
        $legend = $fieldset.querySelector('legend');
      }

      if ($legend && !$legend.querySelector('.delete')) {
        $legend.insertAdjacentHTML('beforeend', '<span class="delete" style="float: right; cursor: pointer" onclick="this.closest(\'.fieldset\').remove()">&nbsp;&times;&nbsp;</span>');
      }
    }

  };

})(Drupal, drupalSettings);
