# Trucie (True Content Import)

Trucie module provides content import from the spreadsheet file. Main features:
* Supported formats: xlsx, xls, ods, csv.
* Provides UI to configure the import (this can be exported to site config).
* Provides basic processing rules for fields from the source file.
* Supports ANY entity type including custom entities (not just nodes and
  taxonomy terms).
* Provides events (similar to hooks) to make it possible to write custom
  data processing.

There are number of modules available that provides content import from xlsx or
csv, the main difference is that Trucie supports all entity types and not just
the ones that comes from core (nodes, terms, etc). Additionally, it's possible
to configure basic processing rules for fields (like floatval or preg_replace).

This module is inspired by Migrate API and Feeds module. But with Migrate API
you have to manually upload the source file to some directory on the server
in order to do the content import, while with Trucie you can just select the
file on the form to upload it.

It requires basic understanding of how entities are structured, so it's
recommended to have a tech-savvy guy to configure the imports. But once it's
set up any one can do the csv import at any time.

## How to create a file to import

Well the xlsx / ods / csv file can be easily created as usual, you should just
keep in mind that:
1. The first line is the header, and it's required.
2. The header should have entity field machine names (just like in DB table).
3. There could be additional fields (that are not part of the entity) that could
   be used in custom processing. It's recommended to start their name with
   a `#`, for example: `#related_entity_id`, `#magic_hash`, etc.

Check some examples below.

## Examples

### Import articles

1. Create an importer (`/admin/structure/trucie_importer/add`).
2. Select entity type and bundle, select the import mode.
3. Check CSV Settings tab if importing from csv, otherwise you can ignore that.
4. Enter the Unique Field (under the Processors tab) - the machine name of the
   field that is used to identify the entity. It can be a `label`, `nid`, `uuid`
   or any other suitable field (the source file should have respective column).
5. If you want to set the text format, then it makes sense to add
   `body__value` and `body__format` columns to the source file. Otherwise `body`
   is enough.
6. If your articles have tags then you can add them to `field_tags` column,
   multiple values can be separated with some character (i.e. `|`, `/` or `::`).
7. To handle multiple value fields we should add the `explode` processor for
   that field:
   * Under the Processors tab click 'Add field'.
   * Enter the field name - field_tags.
   * Add the 'Processor 1': explode, enter the separator.
   * Add the 'Processor 2': entity lookup, set the entity type - taxonomy_term.
   * Maybe you would want to add another processor to trim() values before do
     the lookup.
8. Save.

So eventually the source file should look like this:
```
"title";"body__value";"body__summary";"body__format";"field_tags"
"Hercule Poirot";"Hercule Poirot is a fictional Belgian detective created by Agatha Christie. Poirot is one of Christie's most famous and long-running characters...";"Hercule Poirot is a fictional Belgian detective created by Agatha Christie.";"basic_html";"Poirot"
"Miss Marple";"Miss Jane Marple is a fictional character in Agatha Christie's crime novels and short stories. Miss Marple lives in the village of St. Mary Mead...";;"full_html";"Novels|Miss Marple|Agatha Christie"
"Tommy & Tappence";"Tommy and Tuppence are two fictional detectives, recurring characters in the work of Agatha Christie. Their full names are Thomas Beresford and his wife Prudence...";"Tommy and Tuppence are two detectives created by Agatha Christie.";"basic_html";
...
```

And importer config should look like this:
```
id: example_articles
label: Articles import example
import_entity_type: node
import_entity_bundle: article
mode: create_update
log: true
params:
  csv:
    delimiter: ;
    enclosure: '"'
processors:
  global:
    unique: title
    trim: '1'
  field:
    -
      name: field_tags
      processors:
        -
          type: explode
          p1: '|'
          p2: ''
        -
          type: trim
          p1: ''
          p2: ''
        -
          type: entity_lookup
          p1: taxonomy_term
          p2: name
```

Keep in mind that related entities like tags would not be created automatically
during the import. So you might want to import them first.

### Import tags

It's similar to the previous example:
1. Create an importer (`/admin/structure/trucie_importer/add`).
2. Select the import mode, select entity type, set entity bundle to 'Read from
   source file' (as we're going to import terms from different vocabularies).
3. Enter the Unique Field (under the Processors tab) - i.e. `name` or `uuid`.
4. In this example we don't need to add any field processors here.

So eventually the source file should look like this:
```
"uuid";"bundle";"name";"description"
"029f60b4-b590-4294-a503-ca5a0ce0b189";"genre";"Novels";"A novel is a relatively long work of narrative fiction, typically written in prose and published as a book."
"e3fdc46d-26ab-4899-a78f-fc86307ab2e2";"characters";"Poirot";
"165a44ae-ebc7-49d3-b30a-8e30f0f64c34";"characters";"Miss Marple";
"3f78bbda-9020-4651-8311-cd301e107281";"authors";"Agatha Christie";
...
```

And importer config should look like this:
```
id: example_tags
label: Tags import example
import_entity_type: taxonomy_term
import_entity_bundle: _get_from_source
mode: create
log: true
params:
  csv:
    delimiter: ;
    enclosure: '"'
processors:
  global:
    unique: uuid
    trim: '1'
  field: {  }
```

We use `uuid` field as identifier here to make it possible to sync terms between
environments.

## Custom processing with event subscribers

Module provides several events fired during the import (see TrucieEvents class):
* PRE_PROCESS_RAW - fired before applying field processors to the raw data
  of the row.
* PRE_PROCESS_DATA - fired before processing the row (creating the entity from
  data array), after field processing rules are applied.
* PRE_SAVE - fired before saving the prepared entity.
* POST_SAVE - fired after imported entity is created/updated.

So you can create your own subscribers to these events to modify data or perform
some additional processing. Feel free to check ExampleEventSubscriber from this
module.

## Custom import form

You can use this module for your own needs for example to implement custom
form to import entities from file. Here's an example:

```php
class FantasticArticlesImportForm extends FormBase {

  // ...

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, ?FormStateInterface $formState): array {
    // ...

    $form['file'] = [
      '#type' => 'file',
      '#title' => 'File',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState): void {
    // Get submitted file.
    $file = $this->getRequest()->files->get('files')['file'];
    $filePath = $file ? $file->getRealPath() : '';
    $fileExtension = $file ? $file->getClientOriginalExtension() : '';

    if (!$filePath) {
      return;
    }

    // We need to pass some params to our importer, the easiest way to do that
    // is to create a trucie importer config first (via UI) and then load it
    // here.
    $importerConfig = $this->entityTypeManager()
      ->getStorage('trucie_importer')
      ->load('my_fantastic_articles');
    $params = $importerConfig->getParams(),

    // Optionally you can use 'dry run' mode - all processing will be performed
    // as usual, but entities will not be created.
    $params['is_dry_run'] = TRUE;

    // Additionally you can provide default values to some fields. These values
    // will be used if field value in the source file is empty.
    $params['defaults'] = [
      'field_genre' => 'Fiction',
      'field_author' => 'Me',
    ];

    // Additionally you can specify overrides for some fields. These values
    // will be set to entities in any case, if respective values are found
    // in the source file they are ignored. This may be useful to prevent
    // user malicious behavior.
    $params['overrides'] = [
      'status' => 1,
      'uuid' => NULL,
      'uid' => NULL,
      'created' => NULL,
      'changed' => NULL,
    ];

    // The file extension argument is not required if file path contains it.
    $importer = \Drupal::service('trucie.factory.importer')->createImporter($filePath, $fileExtension);
    $importer
      ->setImportParams($params)
      ->setProcessors($importerConfig->getProcessors())
      ->setSource($filePath, $importerConfig->getCsvParams())
      ->initBatch()
      ->setBatch();

    // If you want to perform some processing before passing a new batch
    // to drupal, then you may ignore `setBatch()` line above, get batch builder
    // object and use it for your own magic:
    // $importer->getBatchBuilder();
  }

}
```
