<?php declare(strict_types=1);

namespace Drupal\trucie\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * The row data processing event.
 */
class RowEvent extends Event {

  /**
   * The related trucie importer id.
   */
  protected string $importerId;

  /**
   * The row data.
   */
  protected array $data;

  public function __construct(string $importerId, array &$data) {
    $this->importerId = $importerId;
    $this->data = &$data;
  }

  /**
   * Returns related importer id.
   *
   * @return string
   *   The trucie importer id.
   */
  public function getImporterId(): string {
    return $this->importerId;
  }

  /**
   * Returns the row data.
   *
   * @return array
   *   The row data.
   */
  public function &getData(): array {
    return $this->data;
  }

}
