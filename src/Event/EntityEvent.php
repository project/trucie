<?php declare(strict_types=1);

namespace Drupal\trucie\Event;

use Drupal\Core\Entity\EntityInterface;

/**
 * The entity processing event.
 */
class EntityEvent extends RowEvent {

  /**
   * The entity.
   */
  private ?EntityInterface $entity;

  public function __construct(string $importerId, array &$data, ?EntityInterface $entity) {
    parent::__construct($importerId, $data);
    $this->entity = $entity;
  }

  /**
   * Returns the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  public function getEntity(): ?EntityInterface {
    return $this->entity;
  }

}
