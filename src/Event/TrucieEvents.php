<?php declare(strict_types=1);

namespace Drupal\trucie\Event;

/**
 * Defines events for this module.
 */
final class TrucieEvents {

  /**
   * Fired before applying field processors to the raw data of the row.
   */
  const PRE_PROCESS_RAW = 'trucie.pre_process_data_raw';

  /**
   * Fired before processing the row (after field processing rules are applied).
   */
  const PRE_PROCESS_DATA = 'trucie.pre_process_data';

  /**
   * Fired before saving the imported entity.
   */
  const PRE_SAVE = 'trucie.pre_save';

  /**
   * Fired after imported entity is saved.
   */
  const POST_SAVE = 'trucie.post_save';

}
