<?php declare(strict_types=1);

namespace Drupal\trucie\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\trucie\TrucieImporterInterface;

/**
 * Defines the trucie importer config entity type.
 *
 * @ConfigEntityType(
 *   id = "trucie_importer",
 *   label = @Translation("Trucie Importer"),
 *   label_collection = @Translation("Trucie Importers"),
 *   label_singular = @Translation("trucie importer"),
 *   label_plural = @Translation("trucie importers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count trucie importer",
 *     plural = "@count trucie importers",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\trucie\TrucieImporterListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trucie\Form\TrucieImporterForm",
 *       "edit" = "Drupal\trucie\Form\TrucieImporterForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "trucie_importer",
 *   admin_permission = "administer trucie_importer",
 *   links = {
 *     "collection" = "/admin/structure/trucie-importer",
 *     "add-form" = "/admin/structure/trucie-importer/add",
 *     "edit-form" = "/admin/structure/trucie-importer/{trucie_importer}",
 *     "delete-form" = "/admin/structure/trucie-importer/{trucie_importer}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "import_entity_type",
 *     "import_entity_bundle",
 *     "fid",
 *     "mode",
 *     "log",
 *     "params",
 *     "processors",
 *   },
 * )
 */
final class TrucieImporter extends ConfigEntityBase implements TrucieImporterInterface {

  /**
   * The entity id.
   */
  protected string $id;

  /**
   * The entity label.
   */
  protected string $label;

  /**
   * The import entity type id.
   */
  protected string $import_entity_type;

  /**
   * The import entity bundle.
   */
  protected string $import_entity_bundle;

  /**
   * The source file fid.
   */
  protected ?string $fid;

  /**
   * The import mode.
   */
  protected string $mode;

  /**
   * TRUE - import log is enabled.
   */
  protected bool $log;

  /**
   * The importer parameters.
   */
  protected array $params = [];

  /**
   * The field processors.
   */
  protected array $processors = [];

  /**
   * {@inheritDoc}
   */
  public function getImportEntityType(): string {
    return $this->import_entity_type ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getImportEntityBundle(): string {
    return $this->import_entity_bundle ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getFid(): string {
    return $this->fid ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function setFid(string $fid): self {
    $this->fid = $fid;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getMode(): string {
    return $this->mode ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvParams(): array {
    return $this->params['csv'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function setCsvParams(array $params): self {
    $this->params['csv'] = $params;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getProcessors(): array {
    return $this->processors ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function setProcessors(array $processors): self {
    $this->processors = $processors;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvDelimiter(): string {
    return ($this->getCsvParams()['delimiter'] ?? '') ?: ';';
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvEnclosure(): string {
    return $this->getCsvParams()['enclosure'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getDefaultCsvEnclosure(): string {
    return '"';
  }

  /**
   * {@inheritDoc}
   */
  public function isLoggingEnabled(): bool {
    return $this->log ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getParams(): array {
    return [
      'importer_id' => $this->id(),
      'entity_type' => $this->getImportEntityType(),
      'entity_bundle' => $this->getImportEntityBundle(),
      'mode' => $this->getMode(),
      'need_log' => $this->isLoggingEnabled(),
    ];
  }

}
