<?php declare(strict_types=1);

namespace Drupal\trucie\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\trucie\Entity\TrucieImporter;
use Drupal\trucie\Importer\BatchImporterBase;
use Drupal\trucie\Importer\BatchImporterFactoryInterface;
use Drupal\trucie\Processor\TrucieImporterProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trucie Importer form.
 */
final class TrucieImporterForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The entity type bundle info.
   */
  private EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The drupal file system handler.
   */
  private FileSystemInterface $fileSystem;

  /**
   * The batch entity importer.
   */
  private BatchImporterFactoryInterface $importerFactory;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, FileSystemInterface $fileSystem, BatchImporterFactoryInterface $importerFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->fileSystem = $fileSystem;
    $this->importerFactory = $importerFactory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('file_system'),
      $container->get('trucie.factory.importer'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $formState): array {
    $form = parent::form($form, $formState);
    $entityBundles = $this->getEntityBundles();
    $processors = $formState->getUserInput()['processors'] ?? $this->entity->getProcessors();

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [TrucieImporter::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['import_entity_type'] = [
      '#title' => $this->t('Entity Type'),
      '#type' => 'select',
      '#options' => $this->getEntityTypes('options'),
      '#default_value' => $this->entity->getImportEntityType(),
      '#required' => TRUE,
    ];
    $form['import_entity_bundle'] = [
      '#title' => $this->t('Entity Bundle'),
      '#description' => $this->t('The "@option" means that source file has a @column column and importer should read the bundle from it during the import.', [
        '@option' => $this->t('Read from source file'),
        '@column' => 'bundle',
      ]),
      '#type' => 'select',
      '#options' => $this->makeEntityBundleOptions($entityBundles),
      '#default_value' => $this->entity->getImportEntityBundle(),
      '#required' => TRUE,
    ];
    $form['mode'] = [
      '#title' => $this->t('Import Mode'),
      '#type' => 'radios',
      '#options' => BatchImporterBase::getAvailableImportModes(),
      '#default_value' => $this->entity->getMode(),
      '#required' => TRUE,
    ];
    $form['log'] = [
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('Log messages are added to drupal log (like DB log).'),
      '#type' => 'checkbox',
      '#default_value' => $this->entity->isLoggingEnabled(),
    ];
    $form['file'] = [
      '#title' => $this->t('File'),
      '#type' => 'managed_file',
      '#upload_location' => 'private://trucie',
      '#default_value' => $this->entity->getFid() ? [$this->entity->getFid()] : NULL,
      '#upload_validators' => [
        'FileExtension' => ['extensions' => 'xlsx xls ods csv'],
      ],
    ];

    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['params'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['params']['csv'] = [
      '#type' => 'details',
      '#title' => $this->t('CSV Settings'),
      '#description' => $this->t('These settings are required for csv files only. You can ignore them if your source file is xlsx or ods.'),
      '#group' => 'settings',
    ];
    $form['params']['csv']['delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter'),
      '#default_value' => $this->entity->getCsvDelimiter(),
      '#required' => TRUE,
    ];
    $form['params']['csv']['enclosure'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enclosure'),
      '#default_value' => $this->entity->getCsvEnclosure() ?: ($this->entity->isNew() ? $this->entity->getDefaultCsvEnclosure() : ''),
    ];

    $form['processors'] = [
      '#type' => 'details',
      '#title' => $this->t('Processors', [], ['context' => 'trucie']),
      '#group' => 'settings',
      '#tree' => TRUE,
    ];
    $form['processors']['global']['unique'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique Field'),
      '#description' => $this->t('The machine name of the field from the source file that is used to identify the entity.')
        . '<br/>'
        . $this->t('You can list several fields separated by comma for complex identifiers.'),
      '#default_value' => $processors['global']['unique'] ?? '',
      '#required' => TRUE,
    ];
    $form['processors']['global']['trim'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Trim All'),
      '#description' => $this->t('Apply trim() to all values from the source file.'),
      '#default_value' => $processors['global']['trim'] ?? TRUE,
    ];

    // Add a template field fieldset.
    $this->addFieldFieldset($form);

    foreach ($processors['field'] ?? [] as $i => $processorParams) {
      $processorParams += [
        '#num' => $i + 1,
      ];
      $this->addFieldFieldset($form, $processorParams);
    }

    // Add a template field processor fieldset.
    $this->addFieldProcessorFieldset($form['processors']['field'][0]['processors']);

    // Add a button to add more field processors.
    $form['processors']['add_field'] = [
      '#type' => 'button',
      '#value' => $this->t('Add field'),
      '#attributes' => [
        'name' => '',
        'class' => ['edit-add-field'],
      ],
      '#weight' => 99,
    ];

    $form['#attached']['drupalSettings']['trucieImporter']['processorTypes'] = $this->getProcessorTypes();
    $form['#attached']['drupalSettings']['trucieImporter']['entityTypes'] = $entityBundles;
    $form['#attached']['library'][] = 'trucie/form_trucie_importer';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $formState) {
    $element = parent::actions($form, $formState);

    $element['submit']['#dropbutton'] = 'save';
    $element['import'] = $element['submit'];
    $element['import']['#dropbutton'] = 'save';
    $element['import']['#value'] = $this->t('Save and import');
    $element['import']['#weight'] = 0;
    $element['import']['#submit'][] = '::import';

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    parent::validateForm($form, $formState);

    // We want to validate field processors that are added with js
    // hence are located outside the form data.
    $userInput = $formState->getUserInput();
    $processors = $userInput['processors'] ?? [];
    $processorTypes = $this->getProcessorTypes();
    $ignoreNums = ['0', '00'];

    if (empty($processors['global']['unique'])) {
      $formState->setErrorByName(rand(1, 999999), $this->t('@name field is required.', [
        '@name' => $this->t('Unique Field'),
      ]));
    }

    foreach ($processors['field'] as $num => &$field) {
      // The 0 child is a template fieldset.
      if (in_array((string) $num, $ignoreNums)) {
        unset($processors['field'][$num]);
        continue;
      }

      // Do not save empty fields.
      if (empty($field['name'])) {
        unset($processors['field'][$num]);
        continue;
      }

      foreach ($field['processors'] ?? [] as $i => $processor) {
        // The 0 child is a template fieldset.
        if (in_array((string) $i, $ignoreNums)) {
          unset($field['processors'][$i]);
          continue;
        }

        foreach ($processor as $pname => $value) {
          $type = $processorTypes[$processor['type']] ?? [];

          // Hack prevention.
          if ($pname === 'type' && !$type) {
            unset($field['processors'][$i]);
          }
          // Check if function parameter is required.
          elseif (($type['params'][$pname]['required'] ?? FALSE) && !$value) {
            $formState->setErrorByName(rand(1, 999999), $this->t('Field @fnum: processor @pnum: parameter @param is required.', [
              '@fnum' => $num,
              '@pnum' => $i,
              '@param' => $type['params'][$pname]['title'],
            ]));
          }
        }
      }

      $field['processors'] = array_values($field['processors']);
    }

    $processors['field'] = array_values($processors['field']);
    $userInput['processors'] = $processors;
    $formState->setUserInput($userInput);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $formState): int {
    $userInput = $formState->getUserInput();
    $this->entity->setFid($userInput['file']['fids'] ?? '');
    $this->entity->setProcessors($userInput['processors'] ?? []);
    $result = parent::save($form, $formState);

    $args = ['%label' => $this->entity->label()];
    $message = match($result) {
      \SAVED_NEW => $this->t('Created new importer %label.', $args),
      \SAVED_UPDATED => $this->t('Updated importer %label.', $args),
    };
    $this->messenger()->addStatus($message);
    $formState->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

  /**
   * Form submission handler for the 'import' action.
   *
   * @param array $form
   *   The form data.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Invalid plugin definition exception.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Plugin not found exception.
   */
  public function import(array $form, FormStateInterface $formState): void {
    $file = $this->entity->getFid()
      ? $this->entityTypeManager->getStorage('file')->load($this->entity->getFid())
      : NULL;
    $filePath = $file ? $this->getFileSystem()->realpath($file->getFileUri()) : '';

    if (!$filePath) {
      return;
    }

    $importer = $this->getImporterFactory()->createImporter($filePath);
    $importer
      ->setImportParams($this->entity->getParams())
      ->setProcessors($this->entity->getProcessors())
      ->setSource($filePath, $this->entity->getCsvParams())
      ->initBatch()
      ->setBatch();
  }

  /**
   * Adds the field processor fieldset to the form.
   *
   * @param array $form
   *   The form data.
   * @param array $params
   *   Field processor params.
   */
  private function addFieldFieldset(array &$form, array $params = []): void {
    $num = $params['#num'] ?? 0;

    if (empty($form['processors']['field'])) {
      $form['processors']['field'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['field-container'],
        ],
      ];
    }

    $form['processors']['field'][$num] = [
      '#type' => 'fieldset',
      '#title' => "[$num]",
      '#attributes' => [
        'class' => array_merge(
          ['fieldset-field'],
          $num === 0 ? ['hidden', 'field-template'] : []
        ),
        'data-num' => $num,
      ],
    ];

    $field = 'name';
    $form['processors']['field'][$num][$field] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Name'),
      '#description' => $this->t('The machine name of the field from the source file.'),
      '#default_value' => $params[$field] ?? NULL,
      '#maxlength' => 255,
    ];

    $form['processors']['field'][$num]['processors'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['field-processor-container'],
      ],
    ];

    foreach ($params['processors'] ?? [] as $i => $processorParams) {
      $processorParams += [
        '#num' => $i + 1,
        '#parent_num' => $num,
      ];
      $this->addFieldProcessorFieldset($form['processors']['field'][$num]['processors'], $processorParams);
    }

    // Add a button to add more processors.
    $form['processors']['field'][$num]['add_processor'] = [
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#attributes' => [
        'name' => '',
        'class' => ['edit-add-processor'],
      ],
      '#weight' => 99,
    ];
  }

  /**
   * Adds field value processor to the form data.
   *
   * @param array $processor
   *   The (parent) field processor.
   * @param array $params
   *   The field value processor params.
   */
  private function addFieldProcessorFieldset(array &$processor, array $params = []): void {
    $parentNum = $params['#parent_num'] ?? 0;
    $num = $params['#num'] ?? '00';

    $processor[$num] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Processor', [], ['context' => 'trucie']) . " $num:",
      '#attributes' => [
        'class' => array_merge(
          ['fieldset-field-processor'],
          $num === '00' ? ['hidden', 'field-processor-template'] : []
        ),
      ],
    ];

    $field = 'type';
    $processorTypeId = "processor-{$parentNum}-{$num}-{$field}";
    $processor[$num][$field] = [
      '#type' => 'select',
      '#options' => $this->getProcessorTypes('options'),
      '#default_value' => $params[$field] ?? 'floatval',
      '#attributes' => [
        'id' => $processorTypeId,
      ],
    ];

    $field = 'p1';
    $processor[$num][$field] = [
      '#type' => 'textfield',
      '#title' => 'Param 1',
      '#default_value' => $params[$field] ?? NULL,
      '#maxlength' => 255,
      '#attributes' => [
        'id' => "processor-{$parentNum}-{$num}-{$field}",
        'class' => ['processor-param'],
        'data-param-name' => $field,
      ],
    ];

    $field = 'p2';
    $processor[$num][$field] = [
      '#type' => 'textfield',
      '#title' => 'Param 2',
      '#default_value' => $params[$field] ?? NULL,
      '#maxlength' => 255,
      '#attributes' => [
        'id' => "processor-{$parentNum}-{$num}-{$field}",
        'class' => ['processor-param'],
        'data-param-name' => $field,
      ],
    ];
  }

  /**
   * Returns processor types.
   *
   * @param string $format
   *   The return value format. Available options:
   *   - default - return types data as is,
   *   - options - return types as options (for select).
   *
   * @return array
   *   Available field value processor types.
   */
  private function getProcessorTypes(string $format = ''): array {
    $res = TrucieImporterProcessor::getProcessorTypes();

    if ($format === 'options') {
      array_walk($res, function (&$item) {
        $item = $item['title'];
      });
    }

    return $res;
  }

  /**
   * Returns entity types.
   *
   * @param string $format
   *   The return value format. Available options:
   *   - default - return entity type definitions as is,
   *   - options - return entity types as options (for select).
   * @param bool $needSort
   *   TRUE - sort result.
   *
   * @return array
   *   Entity types data.
   */
  private function getEntityTypes(string $format = '', bool $needSort = TRUE): array {
    $definitions = $this->entityTypeManager->getDefinitions();

    if ($format === 'options') {
      array_walk($definitions, function (&$item) {
        $item = $item->getLabel()->__toString();
      });
    }

    if ($needSort) {
      asort($definitions);
    }

    return $definitions;
  }

  /**
   * Returns entity bundles.
   *
   * @param bool $needSort
   *   TRUE - sort result.
   * @param bool $needSource
   *   TRUE - read bundle from the source file.
   *
   * @return array
   *   Entity bundles data like this: [
   *     entity_type_id1 => [
   *       title => 'Entity type 1 label',
   *       bundles => [
   *         bundle_name_1,
   *         bundle_name_2,
   *         ...
   *       ],
   *     ],
   *     ...
   *   ].
   */
  private function getEntityBundles(bool $needSort = TRUE, bool $needSource = TRUE): array {
    $definitions = $this->entityTypeManager->getDefinitions();
    $res = [];

    foreach ($definitions as $definition) {
      $bundles = $this->getEntityTypeBundleInfo()->getBundleInfo($definition->id());
      array_walk($bundles, function (&$item) {
        $item = $item['label'] . '';
      });

      if ($needSort) {
        asort($bundles);
      }

      if ($needSource && count($bundles) > 1) {
        $bundles = [BatchImporterBase::BUNDLE_GET_FROM_SOURCE => $this->t('Read from source file')] + $bundles;
      }

      $res[$definition->id()] = [
        'title' => $definition->getLabel()->__toString(),
        'bundles' => $bundles,
      ];
    }

    return $res;
  }

  /**
   * Returns entity bundles as options.
   *
   * @param array $bundleData
   *   The entity bundle data. See this::getEntityBundles().
   *
   * @return string[]
   *   Plain entity bundle list as options (for select).
   */
  private function makeEntityBundleOptions(array $bundleData): array {
    $res = [];

    foreach ($bundleData as $data) {
      foreach ($data['bundles'] ?? [] as $name => $label) {
        $res[$name] = $label;
      }
    }

    return $res;
  }

  /**
   * Returns drupal file system.
   *
   * Due to some weird drupal form processing sometimes form class is created,
   * but $this->entityTypeBundleInfo is missing (during ajax calls?), hence we
   * get a fatal error: property must not be accessed before initialization.
   * To work around that let's use this method and call \Drupal::service() to
   * fix empty properties.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The drupal file system service.
   */
  private function getFileSystem(): FileSystemInterface {
    if (empty($this->fileSystem)) {
      // phpcs:ignore
      $this->fileSystem = \Drupal::service('file_system');
    }

    return $this->fileSystem;
  }

  /**
   * Returns bundle info service.
   *
   * Due to some weird drupal form processing sometimes form class is created,
   * but $this->entityTypeBundleInfo is missing (during ajax calls?), hence we
   * get a fatal error: property must not be accessed before initialization.
   * To work around that let's use this method and call \Drupal::service() to
   * fix empty properties.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The entity type bundle info service.
   */
  private function getEntityTypeBundleInfo(): EntityTypeBundleInfoInterface {
    if (empty($this->entityTypeBundleInfo)) {
      // phpcs:ignore
      $this->entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
    }

    return $this->entityTypeBundleInfo;
  }

  /**
   * Returns importer factory.
   *
   * Due to some weird drupal form processing sometimes form class is created,
   * but $this->importerFactory is missing (during ajax calls?), hence we
   * get a fatal error: property must not be accessed before initialization.
   * To work around that let's use this method and call \Drupal::service() to
   * fix empty properties.
   *
   * @return \Drupal\trucie\Importer\BatchImporterFactoryInterface
   *   The batch importer factory.
   */
  private function getImporterFactory(): BatchImporterFactoryInterface {
    if (empty($this->importerFactory)) {
      // phpcs:ignore
      $this->importerFactory = \Drupal::service('trucie.factory.importer');
    }

    return $this->importerFactory;
  }

}
