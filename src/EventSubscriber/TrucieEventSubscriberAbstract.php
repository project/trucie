<?php declare(strict_types=1);

namespace Drupal\trucie\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides additional processing during the entity import.
 */
abstract class TrucieEventSubscriberAbstract implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  abstract public static function getSubscribedEvents(): array;

  /**
   * Returns importers that should be handles by this class.
   *
   * @return string[]
   *   Importer list in a following format:
   *   @code
   *   [
   *     'trucie_importer_id1' => 'trucie_importer_id1',
   *     'trucie_importer_id2' => 'trucie_importer_id2',
   *     ...
   *   ]
   *   @endcode
   */
  abstract protected function getSubscribedImporterIds(): array;

  /**
   * Checks if event is related to a proper importer and should be handled.
   *
   * @param \Drupal\trucie\Event\RowEvent $event
   *   The event.
   */
  protected function validate(Event $event): bool {
    return !empty($this->getSubscribedImporterIds()[$event->getImporterId()]);
  }

}
