<?php declare(strict_types=1);

namespace Drupal\trucie\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\trucie\Event\TrucieEvents;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides additional processing during the entity import.
 */
class ExampleEventSubscriber extends TrucieEventSubscriberAbstract {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      TrucieEvents::PRE_PROCESS_DATA => 'firePreProcessData',
      TrucieEvents::POST_SAVE => 'firePostSave',
    ];
  }

  /**
   * {@inheritDoc}
   */
  protected function getSubscribedImporterIds(): array {
    return [
      'example_importer' => 'example_importer',
    ];
  }

  /**
   * Starts the pre_process_data event handling.
   *
   * @param \Drupal\trucie\Event\RowEvent $event
   *   The event.
   */
  public function firePreProcessData(Event $event): void {
    if ($this->validate($event)) {
      $this->onPreProcessData($event->getData());
    }
  }

  /**
   * Starts the post_save event handling.
   *
   * @param \Drupal\trucie\Event\EntityEvent $event
   *   The event.
   */
  public function firePostSave(Event $event): void {
    if ($this->validate($event)) {
      $this->onPostSave($event->getEntity(), $event->getData());
    }
  }

  /**
   * Handles the pre_process_data event for our importer.
   *
   * @param array $data
   *   The row data.
   */
  private function onPreProcessData(array &$data): void {
    // phpcs:disable
    // Do some magic here.
    //
    // It's possible to mark a row as skipped here by setting a reason in
    // $data['#trucie_skip_row'] like this:
    // if (empty($data['fantastic_requirement'])) {
    //   $data['#trucie_skip_row'] = 'Can not import non fantastic entity.';
    //   return;
    // }
    // phpcs:enable
  }

  /**
   * Handles the post_save event for our importer.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param array $data
   *   The row data.
   */
  private function onPostSave(EntityInterface $entity, array &$data): void {
    // Do more magic here.
  }

}
