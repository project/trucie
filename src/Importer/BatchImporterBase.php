<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\trucie\Event\EntityEvent;
use Drupal\trucie\Event\RowEvent;
use Drupal\trucie\Event\TrucieEvents;
use Drupal\trucie\Processor\TrucieImporterProcessorInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides base batch entity import stuff.
 */
abstract class BatchImporterBase implements BatchImporterInterface {

  use StringTranslationTrait;

  /**
   * The importer processor service name.
   */
  protected string $importerProcessorName = '';

  /**
   * The field value processing rules.
   */
  protected array $processors = [];

  /**
   * The import params.
   */
  protected array $importParams = [];

  /**
   * The source file parse params.
   */
  protected array $sourceParams = [];

  /**
   * The source file type.
   */
  protected string $sourceType = '';

  /**
   * The source file path.
   */
  protected string $sourcePath = '';

  /**
   * The batch id.
   */
  protected string $batchId;

  /**
   * The batch size.
   */
  protected int $batchSize = 10;

  public function __construct(string $importerProcessorName = '') {
    $this->setImporterProcessorName($importerProcessorName ?: 'trucie.processor');
    $this->initBatchBuilder();
  }

  /**
   * Sets the importer processor name.
   *
   * @param string $importerProcessorName
   *   The importer processor service name.
   *
   * @return $this
   */
  public function setImporterProcessorName(string $importerProcessorName): self {
    $this->importerProcessorName = $importerProcessorName;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setImportParams(array $params): self {
    $this->importParams = $params;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setProcessors(array $processors): self {
    $this->processors = $processors;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getSourceType(string $format = ''): string {
    return $format === 'ucfirst' ? ucfirst($this->sourceType) : $this->sourceType;
  }

  /**
   * {@inheritDoc}
   */
  public function setSourceType(string $type): self {
    $this->sourceType = strtolower($type);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setSource(string $path, array $params): self {
    $this->sourcePath = $path;
    $this->sourceParams = $params;
    $this->readSource();

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function &getBatchBuilder(): BatchBuilder {
    return $GLOBALS['_trucie']['batch_builder'][$this->batchId];
  }

  /**
   * {@inheritDoc}
   */
  public function initBatch(): self {
    $this->getBatchBuilder()
      ->setFile($this->getModuleHandler()->getModule('trucie')->getPath() . '/src/Importer/' . basename(str_replace('\\', '/', static::class . '.php')))
      ->setFinishCallback([$this, 'finished']);

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setBatch(): self {
    batch_set($this->getBatchBuilder()->toArray());
    return $this;
  }

  /**
   * Handles source file item processing.
   *
   * This method is kind of a wrapper for $this->processItem(). As number of
   * columns in the source file may vary it's not easy to list all of them
   * as function arguments. That's why here we use func_get_args() to get them
   * and pass to $this->processItem().
   */
  public function processOperation(): self {
    $args = func_get_args();
    $context = end($args);
    $rows = reset($args);

    foreach ($rows['rows'] as $data) {
      $this->dispatchRowEvent(TrucieEvents::PRE_PROCESS_RAW, $data);
      $this->preprocessItem($data);

      $this->dispatchRowEvent(TrucieEvents::PRE_PROCESS_DATA, $data);
      $entity = $this->processItem($data, $context);

      if ($entity && !$this->isDryRun()) {
        $this->dispatchEntityEvent(TrucieEvents::PRE_SAVE, $data, $entity);
        $entity->save();
        $this->dispatchEntityEvent(TrucieEvents::POST_SAVE, $data, $entity);
      }
    }

    return $this;
  }

  /**
   * Performs field values preprocessing according to processing rules.
   *
   * @param string[] $data
   *   The row data.
   *
   * @return $this
   */
  protected function preprocessItem(array &$data): self {
    if (!$importerProcessor = $this->getImporterProcessor()) {
      return $this;
    }

    // Set default values to some fields.
    foreach ($this->getFieldDefaults() as $name => $defaultValue) {
      $value = (string) ($data[$name] ?? '');
      $data[$name] = $value === '' ? $defaultValue : $value;
    }

    // Override some values.
    foreach ($this->getFieldOverrides() as $name => $value) {
      $data[$name] = $value;
    }

    // Apply global processing rules.
    if ($this->getGlobalProcessors()['trim'] ?? FALSE) {
      $data = $importerProcessor->trimAll($data);
    }

    // Apply field processing rules.
    foreach ($this->getFieldProcessors() as $processor) {
      if (isset($data[$processor['name']])) {
        $data[$processor['name']] = $importerProcessor->process((string) $data[$processor['name']], $processor['processors'] ?? []);
      }
    }

    // Remove empty values to avoid sql issues on entity insert.
    foreach ($data as $field => $value) {
      if (!isset($value)) {
        unset($data[$field]);
      }
    }

    // Alter bundle and field names if needed.
    $this
      ->preprocessItemEntityBundle($data)
      ->preprocessItemFieldNames($data);

    return $this;
  }

  /**
   * Performs entity bundle processing.
   *
   * @param array $data
   *   The row data.
   *
   * @return $this
   */
  protected function preprocessItemEntityBundle(array &$data): self {
    // Specify entity bundle (if configured).
    $entityBundle = $this->getImportEntityBundle();
    if ($entityBundle && $entityBundle !== self::BUNDLE_GET_FROM_SOURCE) {
      $data['bundle'] = $entityBundle;
    }

    // Some entities have different field name for bundle.
    if (!empty($data['bundle'])) {
      $bundleField = '';

      switch ($this->getImportEntityType()) {
        case 'node':
          $bundleField = 'type';
          break;

        case 'taxonomy_term':
          $bundleField = 'vid';
          break;
      }

      if ($bundleField) {
        $data[$bundleField] = $data['bundle'];
        unset($data['bundle']);
      }
    }

    return $this;
  }

  /**
   * Performs field names preprocessing.
   *
   * There may be fields like body that actually has a value and a format.
   * So to make it possible to create entity from array this method may be used
   * to replace values like this:
   * @code
   * $data => [
   *   ...
   *   'body__value' => 'What a wonderful world!',
   *   'body__format' => 'basic_html',
   *   ...
   * ]
   * @endcode
   *
   * With this:
   * @code
   * $data => [
   *   ...
   *   'body' => [
   *     'value' => 'What a wonderful world!',
   *     'format' => 'basic_html',
   *   ],
   *   ...
   * ]
   * @endcode
   *
   * @param array $data
   *   The row data.
   *
   * @return $this
   */
  protected function preprocessItemFieldNames(array &$data): self {
    $replace = [];
    $separator = '__';
    // We are looking for fields with suffix like __value or __target_id.
    $compositeFields = ['value', 'target_id'];

    // Find composite fields.
    foreach ($data as $key => $value) {
      if (strpos($key, $separator)) {
        $field = explode($separator, $key);
        if (count($field) === 2) {
          $replace[$field[0]][$field[1]] = $field[1];

          if (in_array($field[1], $compositeFields)) {
            $replace[$field[0]]['#key'] = $field[1];
          }
        }
      }
    }

    // Replace the fields.
    foreach ($replace as $field => $list) {
      // If we face some weird custom field (that is not related to __value or
      // __target_id) then just ignore it.
      if (empty($list['#key'])) {
        continue;
      }

      unset($list['#key']);

      foreach ($list as $fieldSuffix) {
        $data[$field][$fieldSuffix] = $data[$field . $separator . $fieldSuffix];
        unset($data[$field . $separator . $fieldSuffix]);
      }
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  abstract public function processItem(array $data, array &$context): ?EntityInterface;

  /**
   * {@inheritDoc}
   */
  public function finished($success, $results, $operations): void {
    $this->getMessenger()->addStatus($this->t('Entities created: @count.', [
      '@count' => $results['create'] ?? 0,
    ]));
    $this->getMessenger()->addStatus($this->t('Entities updated: @count.', [
      '@count' => $results['update'] ?? 0,
    ]));
    $this->getMessenger()->addStatus($this->t('Entities skipped: @count.', [
      '@count' => $results['skip'] ?? 0,
    ]));

    if (!$success) {
      $this->getMessenger()->addError($this->t('Import finished with an error.'));
    }
  }

  /**
   * Returns available import modes.
   *
   * @return string[]
   *   The available import modes.
   */
  public static function getAvailableImportModes(): array {
    return [
      self::MODE_CREATE => t('Create new entities, skip existing entities'),
      self::MODE_CREATE_UPDATE => t('Create new entities, update existing entities'),
    ];
  }

  /**
   * Reads the items to import from the source file.
   *
   * @return $this
   */
  abstract protected function readSource(): self;

  /**
   * Adds a batch item to import.
   *
   * @param array $rows
   *   Bunch of rows from the source file.
   *
   * @return $this
   */
  protected function addOperation(array $rows): self {
    $this->getBatchBuilder()->addOperation([$this, 'processOperation'], [$rows]);
    return $this;
  }

  /**
   * Update entity fields from array.
   *
   * @param array $data
   *   The entity fields.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity. If empty then new entity is created.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The updated/created entity.
   */
  protected function updateEntityFromArray(array $data, ?EntityInterface $entity, EntityStorageInterface $storage): EntityInterface {
    if ($entity) {
      foreach ($data as $field => $value) {
        if (substr($field, 0, 1) === '#') {
          continue;
        }

        $entity->__set($field, $value);
      }
    }
    else {
      $entity = $storage->create($data);
    }

    return $entity;
  }

  /**
   * Returns related importer id.
   *
   * @return string
   *   The trucie importer id.
   */
  protected function getImporterId(): string {
    return $this->importParams['importer_id'] ?? '';
  }

  /**
   * Returns import entity type.
   *
   * @return string
   *   The entity type machine name.
   */
  protected function getImportEntityType(): string {
    return $this->importParams['entity_type'] ?? '';
  }

  /**
   * Returns import entity bundle.
   *
   * @return string
   *   May return one of the following:
   *   - The entity bundle machine name.
   *   - If entity type does not have bundles then empty string is returned.
   *   - If a source file has (should have) a bundle column
   *     then BUNDLE_GET_FROM_SOURCE constant is returned.
   */
  protected function getImportEntityBundle(): string {
    $bundle = $this->importParams['entity_bundle'] ?? '';
    return $bundle === $this->getImportEntityType() ? '' : $bundle;
  }

  /**
   * Returns import mode.
   *
   * @return string
   *   The import mode.
   *
   * @see self::getAvailableImportModes()
   */
  protected function getImportMode(): string {
    return $this->importParams['mode'] ?? self::MODE_CREATE;
  }

  /**
   * Shows if import logging is enabled.
   *
   * @return bool
   *   TRUE if logging is enabled, otherwise FALSE.
   */
  protected function isLoggingEnabled(): bool {
    return $this->importParams['need_log'] ?? FALSE;
  }

  /**
   * Shows if dry run is enabled (entities would not be saved).
   *
   * @return bool
   *   TRUE if dry run is enabled, otherwise FALSE.
   */
  private function isDryRun(): bool {
    return $this->importParams['is_dry_run'] ?? FALSE;
  }

  /**
   * Returns global processors' data.
   *
   * @return array
   *   The global processors' data.
   */
  protected function getGlobalProcessors(): array {
    return $this->processors['global'] ?? [];
  }

  /**
   * Returns field processors' data.
   *
   * @return array
   *   The field processors' data.
   */
  protected function getFieldProcessors(): array {
    return $this->processors['field'] ?? [];
  }

  /**
   * Returns default values for empty fields of imported entities.
   *
   * These defaults will be applied to fields with empty value. Keep in mind
   * that 0 is not considered to be empty.
   *
   * @return array
   *   The default values for empty fields as name => value.
   */
  protected function getFieldDefaults(): array {
    return $this->importParams['defaults'] ?? [];
  }

  /**
   * Returns field overrides.
   *
   * @return array
   *   The fields to override as name => value.
   */
  protected function getFieldOverrides(): array {
    return $this->importParams['overrides'] ?? [];
  }

  /**
   * Returns the name of the unique fields from the source file.
   *
   * @return string[]
   *   The machine names of fields from the source file
   *   that are used to identify the entity.
   */
  protected function getUniqueFields(): array {
    $fields = explode(',', $this->getGlobalProcessors()['unique'] ?? '');
    array_walk($fields, function(&$item) {
      $item = trim($item);
    });

    return $fields;
  }

  /**
   * Checks if unique fields are set in entity data.
   *
   * @param array $data
   *   The row data.
   *
   * @return bool
   *   FALSE if any of key fields is empty, TRUE otherwise.
   */
  protected function checkUniqueFields(array $data): bool {
    foreach ($this->getUniqueFields() as $field) {
      if (!trim((string) ($data[$field] ?? ''))) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Finds existing entity by its unique fields.
   *
   * @param array $data
   *   The row data.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Invalid plugin definition exception.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Plugin not found exception.
   */
  protected function findEntity(array $data): ?EntityInterface {
    $properties = [];

    foreach ($this->getUniqueFields() as $field) {
      $properties[$field] = $data[$field];
    }

    $entity = $this->getStorage()->loadByProperties($properties) ?: [];
    $entity = reset($entity);

    return $entity ?: NULL;
  }

  /**
   * Returns entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Invalid plugin definition exception.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Plugin not found exception.
   */
  protected function getStorage(): EntityStorageInterface {
    return $this->getEntityTypeManager()->getStorage($this->getImportEntityType());
  }

  /**
   * Adds a message to the import log.
   *
   * @param string $type
   *   The message type.
   *   Allowed values: status, warning, error.
   * @param string $message
   *   The message.
   * @param array $details
   *   Message metadata like row num.
   */
  private function log(string $type, string $message, array $details = []): void {
    if ($this->isLoggingEnabled()) {
      $details['row'] = !empty($details['row']) ? 'row=' . print_r($details['row'], TRUE) : '';
      $meta = ($details['op'] ?? '') ? $details['op'] . ':' : '';
      $meta .= $details['row_num'] ?? '';
      $meta = "[$type]" . ($meta ? "[$meta]" : "");
      $this->getLogger()->debug("{$meta} {$message} <pre>@row</pre>.", [
        '@row' => $details['row'],
      ]);
    }
  }

  /**
   * Adds an error message to the import log.
   *
   * @param string $message
   *   The message.
   * @param array $details
   *   Message metadata like row num.
   */
  protected function logError(string $message, array $details = []): void {
    $this->log('error', $message, $details);
  }

  /**
   * Adds a status message to the import log.
   *
   * @param string $message
   *   The message.
   * @param array $details
   *   Message metadata like row num.
   */
  protected function logStatus(string $message, array $details = []): void {
    $this->log('status', $message, $details);
  }

  /**
   * Dispatches the row data processing event.
   *
   * @param string $eventName
   *   The event name.
   * @param array $row
   *   The row data.
   */
  private function dispatchRowEvent(string $eventName, array &$row): void {
    $event = new RowEvent($this->getImporterId(), $row);
    $this->getEventDispatcher()->dispatch($event, $eventName);
  }

  /**
   * Dispatches the entity processing event.
   *
   * @param string $eventName
   *   The event name.
   * @param array $row
   *   The row data.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  private function dispatchEntityEvent(string $eventName, array &$row, EntityInterface $entity): void {
    $event = new EntityEvent($this->getImporterId(), $row, $entity);
    $this->getEventDispatcher()->dispatch($event, $eventName);
  }

  /**
   * Returns importer processor.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Drupal\trucie\Processor\TrucieImporterProcessorInterface|null
   *   The importer processor.
   */
  protected function getImporterProcessor(): ?TrucieImporterProcessorInterface {
    return $this->importerProcessorName ? \Drupal::service($this->importerProcessorName) : NULL;
  }

  /**
   * Returns entity type manager.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::entityTypeManager();
  }

  /**
   * Returns module handler.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected function getModuleHandler(): ModuleHandlerInterface {
    return \Drupal::moduleHandler();
  }

  /**
   * Returns messenger.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   The drupal messenger.
   */
  protected function getMessenger(): MessengerInterface {
    return \Drupal::messenger();
  }

  /**
   * Returns logger.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  protected function getLogger(): LoggerInterface {
    return \Drupal::logger('trucie');
  }

  /**
   * Returns event dispatcher.
   *
   * Note: we have to avoid using dependency injection here as it require either
   * to have a __sleep() method inside dependency or to use static methods
   * for batch processing. Both options can not be satisfied in full
   * so let's go ahead with \Drupal calls.
   *
   * @return \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   *   The event dispatcher.
   */
  protected function getEventDispatcher(): ContainerAwareEventDispatcher {
    return \Drupal::service('event_dispatcher');
  }

  /**
   * Inits batch builder.
   *
   * Initially we had a $batchBuilder property in this class but this caused
   * some side effects as we used it like this:
   * @code
   * $this->batchBuilder->addOperation([$this, 'processOperation'], [$rows]);
   * @endcode
   *
   * The `[$this, 'processOperation']` part means that $this will be serialized
   * before saving to db batch table. More than that - every item added will be
   * a part of this class instance hence also will be serialized.
   * If the source file is big then this class is going to store all of it rows,
   * the serialization is going to take much time and lots of space is going
   * to be used in db.
   * That's why we're going to store the batch builder as a global variable
   * (move outside this class), this will be enough to put the data to the db
   * and launch batch processing.
   */
  private function initBatchBuilder(): void {
    $this->batchId = md5(time() . rand());
    $GLOBALS['_trucie']['batch_builder'][$this->batchId] = new BatchBuilder();
  }

}
