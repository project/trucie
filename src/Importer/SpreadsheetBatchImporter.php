<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;

/**
 * Provides batch entity import from xlsx / ods file.
 */
class SpreadsheetBatchImporter extends CsvBatchImporter {

  /**
   * {@inheritDoc}
   */
  protected function readSource(): self {
    $spreadsheet = $this->sourcePath ? $this->loadSpreadsheet($this->sourcePath) : NULL;
    $sheet = $spreadsheet ? $spreadsheet->getSheet(0) : NULL;
    $headerSize = 0;
    $num = 0;
    $count = 0;
    $rows = [];
    $header = [];

    if ($sheet) {
      foreach ($sheet->getRowIterator() as $row) {
        $data = $this->getRow($row);
        $meta = $data['#trucie_meta'];
        unset($data['#trucie_meta']);

        // Treat the first row as a table header.
        // Also remove nulls from the end of the array.
        if (empty($header)) {
          while (end($data) === NULL) {
            array_pop($data);
          }

          $header = $data;
          $headerSize = count($header);
          continue;
        }

        $dataSize = count($data);

        // Make sure that header and row data has the same size.
        if ($headerSize > $dataSize) {
          $data = array_pad($data, $headerSize, NULL);
          $meta = array_pad($meta, $headerSize, NULL);
        }
        elseif ($headerSize < $dataSize) {
          $data = array_slice($data, 0, $headerSize, TRUE);
          $meta = array_slice($meta, 0, $headerSize, TRUE);
        }

        $data = array_combine($header, $data);
        $meta = array_combine($header, $meta);
        $data['#trucie_row_num'] = ++$num;
        $data['#trucie_meta'] = $meta;
        $rows[] = $data;

        if (++$count === $this->batchSize) {
          $this->addOperation(['rows' => $rows]);
          $rows = [];
          $count = 0;
        }
      }

      if ($rows) {
        $this->addOperation(['rows' => $rows]);
      }
    }

    if (!$num) {
      $pathInfo = pathinfo($this->sourcePath);
      $this->getMessenger()->addError($this->t('File @file is empty or not readable.', [
        '@file' => $pathInfo['basename'],
      ]));
    }

    return $this;
  }

  /**
   * Returns row data from the spreadsheet.
   *
   * @param \PhpOffice\PhpSpreadsheet\Worksheet\Row $row
   *   A table row.
   *
   * @return array
   *   The table row data.
   */
  private function getRow(Row $row): array {
    $data = [];
    $meta = [];

    foreach ($row->getCellIterator() as $cell) {
      $code = $cell->getStyle()->getNumberFormat()->getFormatCode();
      $value = $cell->getFormattedValue();

      // Fix date string to make it parsable by strtotime().
      if ($code === 'dd/mm/yyyy') {
        $value = str_replace('/', '.', $value);
      }

      $data[] = $value;
      $meta[] = ['format_code' => $code];
    }

    $data['#trucie_meta'] = $meta;

    return $data;
  }

  /**
   * Loads template spreadsheet to export data to.
   *
   * @param string $filePath
   *   A file path.
   *
   * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
   *   The loaded spreadsheet.
   */
  private function loadSpreadsheet(string $filePath): Spreadsheet {
    $reader = IOFactory::createReader($this->getSourceType('ucfirst'));
    return $reader->load($filePath);
  }

}
