<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Factory to create trucie importer.
 */
class BatchImporterFactory implements BatchImporterFactoryInterface {

  public function __construct(private ContainerInterface $container) {
  }

  /**
   * {@inheritDoc}
   */
  public function createImporter(string $fileName, string $fileExtension = ''): BatchImporterInterface {
    $fileExtension = strtolower($fileExtension ?: pathinfo($fileName)['extension']);
    $importer = '';

    switch ($fileExtension) {
      case 'xlsx':
      case 'xls':
      case 'ods':
        $importer = 'trucie.importer.spreadsheet_batch';
        break;

      case 'csv':
        $importer = 'trucie.importer.csv_batch';
        break;
    }

    if (!$importer) {
      throw new \Exception("No importer found for type '$fileExtension'.");
    }

    /** @var \Drupal\trucie\Importer\BatchImporterInterface $res */
    $res = $this->container->get($importer);
    $res->setSourceType($fileExtension);

    return $res;
  }

}
