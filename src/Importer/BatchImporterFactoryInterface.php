<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

/**
 * Interface for factory to create trucie importer.
 */
interface BatchImporterFactoryInterface {

  /**
   * Creates importer for specified file.
   *
   * @param string $fileName
   *   The full file name.
   *   Is used to guess the importer type.
   * @param string $fileExtension
   *   The file extension.
   *   May be set if $fileName has no extension.
   *
   * @return \Drupal\trucie\Importer\BatchImporterInterface
   *   The trucie batch importer instance.
   *
   * @throws \Exception
   *   No importer found exception.
   */
  public function createImporter(string $fileName, string $fileExtension = ''): BatchImporterInterface;

}
