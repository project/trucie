<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides batch entity importer interface.
 */
interface BatchImporterInterface {

  /**
   * The import mode: Create new entities, skip existing entities.
   */
  const MODE_CREATE = 'create';

  /**
   * The import mode: create new entities, update existing entities.
   */
  const MODE_CREATE_UPDATE = 'create_update';

  /**
   * The bundle value in importer settings telling us to get bundle from source.
   */
  const BUNDLE_GET_FROM_SOURCE = '_get_from_source';

  /**
   * Returns source file type.
   *
   * @param string $format
   *   The return value format.
   *   Available options:
   *   - default: the file type in lower case,
   *   - ucfirst: the file type with the first capital letter.
   *
   * @return string
   *   The source file extension.
   */
  public function getSourceType(string $format = ''): string;

  /**
   * Sets source file type.
   *
   * @param string $type
   *   The source file type/extension.
   *
   * @return $this
   */
  public function setSourceType(string $type): self;

  /**
   * Sets import params.
   *
   * @param array $params
   *   The import params.
   *
   * @return $this
   */
  public function setImportParams(array $params): self;

  /**
   * Sets field values processors.
   *
   * @param array $processors
   *   The field value processing rules.
   *
   * @return $this
   */
  public function setProcessors(array $processors): self;

  /**
   * Sets the source file and its params.
   *
   * @param string $path
   *   The source file path.
   * @param array $params
   *   The file parse params.
   *
   * @return $this
   */
  public function setSource(string $path, array $params): self;

  /**
   * Handles source file item processing.
   *
   * @param array $data
   *   The row from the source file.
   * @param array $context
   *   The batch context.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity created from data array.
   */
  public function processItem(array $data, array &$context): ?EntityInterface;

  /**
   * The batch finish callback.
   *
   * @param bool $success
   *   Shows if batch processing completed successfully.
   * @param array $results
   *   Batch results data.
   * @param array $operations
   *   Batch operations.
   */
  public function finished($success, $results, $operations): void;

  /**
   * Initializes the batch.
   *
   * @return $this
   */
  public function initBatch(): self;

  /**
   * Adds a new batch to drupal.
   *
   * @return $this
   */
  public function setBatch(): self;

  /**
   * Returns the batch builder.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The batch builder.
   *
   * @see self::initBatchBuilder()
   */
  public function getBatchBuilder(): BatchBuilder;

}
