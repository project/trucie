<?php declare(strict_types=1);

namespace Drupal\trucie\Importer;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides batch entity import from csv file.
 */
class CsvBatchImporter extends BatchImporterBase {

  /**
   * {@inheritDoc}
   */
  protected function readSource(): self {
    $num = 0;
    $count = 0;
    $rows = [];
    $header = [];

    if ($this->sourcePath && ($handle = fopen($this->sourcePath, 'r'))) {
      while (($row = fgetcsv($handle, 0, $this->sourceParams['delimiter'] ?: ';', $this->sourceParams['enclosure'] ?? '"')) !== FALSE) {
        // Treat the first row as a table header.
        if (empty($header)) {
          $header = $row;
          continue;
        }

        // Make assoc array out of data.
        $row = array_combine($header, $row);

        $row['#trucie_row_num'] = ++$num;
        $rows[] = $row;

        if (++$count === $this->batchSize) {
          $this->addOperation(['rows' => $rows]);
          $rows = [];
          $count = 0;
        }
      }

      if ($rows) {
        $this->addOperation(['rows' => $rows]);
      }

      fclose($handle);
    }

    if (!$num) {
      $pathInfo = pathinfo($this->sourcePath);
      $this->getMessenger()->addError($this->t('File @file is empty or not readable.', [
        '@file' => $pathInfo['basename'],
      ]));
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function processItem(array $data, array &$context): ?EntityInterface {
    $mode = $this->getImportMode();
    $meta = ['row_num' => $data['#trucie_row_num']];
    $op = 'skip';

    if (!empty($data['#trucie_skip_row'])) {
      $context['results'][$op] = ($context['results'][$op] ?? 0) + 1;
      $this->logError(
        $data['#trucie_skip_row'],
        $meta + ['op' => $op]
      );
      return NULL;
    }

    if (!$this->checkUniqueFields($data)) {
      $context['results'][$op] = ($context['results'][$op] ?? 0) + 1;
      $this->logError(
        'The unique field is empty.',
        $meta + ['op' => $op]
      );
      return NULL;
    }

    $entity = $this->findEntity($data);
    $exists = !empty($entity);

    if ($entity && $mode === 'create') {
      $context['results'][$op] = ($context['results'][$op] ?? 0) + 1;
      $this->logStatus(
        "The entity already exists (id={$entity->id()}).",
        $meta + ['op' => $op]
      );
      return NULL;
    }

    $entity = $this->updateEntityFromArray($data, $entity, $this->getStorage());
    $op = $exists ? 'update' : 'create';

    $message = "id={$entity->id()}: {$entity->label()}";
    $context['results'][$op] = ($context['results'][$op] ?? 0) + 1;
    $context['message'] = $entity->label();
    $this->logStatus(
      $message,
      $meta + ['op' => $op, 'row' => $data]
    );

    return $entity;
  }

}
