<?php declare(strict_types=1);

namespace Drupal\trucie\Processor;

/**
 * Provides an interface for field value processing.
 */
interface TrucieImporterProcessorInterface {

  /**
   * Returns processor types.
   *
   * @return array
   *   Field value processor type metadata.
   */
  public static function getProcessorTypes(): array;

  /**
   * Performs field value processing.
   *
   * @param string $value
   *   The field value.
   * @param array $processors
   *   The processors array (processing rules) like this: [
   *     [
   *       type => 'mb_strtolower',
   *     ],
   *     [
   *       type => preg_replace,
   *       p1: '/\s+/',
   *       p2: '-'
   *     ],
   *     ...
   *   ].
   *
   * @return mixed
   *   Processed field value.
   */
  public function process(string $value, array $processors): mixed;

}
