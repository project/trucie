<?php declare(strict_types=1);

namespace Drupal\trucie\Processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Handles field value processing.
 */
class TrucieImporterProcessor implements TrucieImporterProcessorInterface {

  /**
   * The entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getProcessorTypes(): array {
    return [
      'date' => [
        'name' => 'date',
        'title' => 'date()',
        'params' => [
          'p1' => [
            'title' => 'Date Format',
            'required' => TRUE,
          ],
        ],
      ],
      'explode' => [
        'name' => 'explode',
        'title' => 'explode()',
        'params' => [
          'p1' => [
            'title' => 'Separator',
          ],
        ],
      ],
      'floatval' => [
        'name' => 'floatval',
        'title' => 'floatval()',
        'params' => [
          'p1' => [
            'title' => 'Round Precision',
          ],
        ],
      ],
      'implode' => [
        'name' => 'implode',
        'title' => 'implode()',
        'params' => [
          'p1' => [
            'title' => 'Separator',
          ],
        ],
      ],
      'intval' => [
        'name' => 'intval',
        'title' => 'intval()',
      ],
      'mb_strtolower' => [
        'name' => 'mb_strtolower',
        'title' => 'mb_strtolower()',
      ],
      'mb_strtoupper' => [
        'name' => 'mb_strtoupper',
        'title' => 'mb_strtoupper()',
      ],
      'preg_replace' => [
        'name' => 'preg_replace',
        'title' => 'preg_replace()',
        'params' => [
          'p1' => [
            'title' => 'Pattern',
            'required' => TRUE,
          ],
          'p2' => [
            'title' => 'Replacement',
            'required' => TRUE,
          ],
        ],
      ],
      'strtotime' => [
        'name' => 'strtotime',
        'title' => 'strtotime()',
      ],
      'trim' => [
        'name' => 'trim',
        'title' => 'trim()',
        'params' => [
          'p1' => [
            'title' => 'Characters',
          ],
        ],
      ],
      'ucfirst' => [
        'name' => 'ucfirst',
        'title' => 'ucfirst()',
      ],
      'entity_lookup' => [
        'name' => 'entity_lookup',
        'title' => 'Entity Lookup',
        'params' => [
          'p1' => [
            'title' => 'Entity Type',
            'required' => TRUE,
          ],
          'p2' => [
            'title' => 'Lookup Field Name',
            'required' => TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * Applies trim() to each item in data array.
   *
   * @param string[] $data
   *   The row data.
   *
   * @return string[]
   *   The processed data.
   */
  public function trimAll(array $data): array {
    foreach ($data as &$value) {
      if (!is_array($value) && !is_null($value)) {
        $value = trim((string) $value);
      }
    }

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function process(string $value, array $processors): mixed {
    foreach ($processors as $processor) {
      $method = ($processor['type'] ?? '') ? 'process' . $this->makeMethodName($processor['type']) : '';
      if ($method && method_exists($this, $method)) {
        $value = $this->{$method}($value, $processor);
      }
      else {
        throw new \Exception("The processor '{$processor['type']}' is not supported yet.");
      }
    }

    return $value;
  }

  /**
   * Applies the explode() function to the value.
   *
   * @param string|array $value
   *   A value to process.
   * @param array $params
   *   Function params.
   *
   * @return array
   *   The processed value.
   */
  private function processExplode($value, array $params): array {
    return !is_array($value) ? explode($params['p1'], $value) : $value;
  }

  /**
   * Applies the implode() function to the value.
   *
   * @param string|array $value
   *   A value to process.
   * @param array $params
   *   Function params.
   *
   * @return string
   *   The processed value.
   */
  private function processImplode($value, array $params): string {
    return is_array($value) ? implode($params['p1'], $value) : (string) $value;
  }

  /**
   * Applies the floatval() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return float|array
   *   The processed value (or array of values).
   */
  private function processFloatval($value, array $params): mixed {
    // This function will perform the floatval() and round() if needed.
    $func = function ($value, $precision) {
      $value = (float) str_replace(',', '.', $value);
      if ($precision) {
        $value = round($value, (int) $precision);
      }

      return $value;
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v, $params['p1'] ?? '');
      }
    }
    else {
      $value = $func($value, $params['p1'] ?? '');
    }

    return $value;
  }

  /**
   * Applies the floatval() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return int|array
   *   The processed value (or array of values).
   */
  private function processIntval($value, array $params): mixed {
    // This function will perform the intval().
    $func = function ($value) {
      return (int) $value;
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v);
      }
    }
    else {
      $value = $func($value);
    }

    return $value;
  }

  /**
   * Applies the mb_strtolower() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processMbStrtolower($value, array $params): mixed {
    // This function will perform the mb_strtolower().
    $func = function ($value) {
      return mb_strtolower((string) $value);
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v);
      }
    }
    else {
      $value = $func($value);
    }

    return $value;
  }

  /**
   * Applies the mb_strtoupper() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processMbStrtoupper($value, array $params): mixed {
    // This function will perform the mb_strtoupper().
    $func = function ($value) {
      return mb_strtoupper((string) $value);
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v);
      }
    }
    else {
      $value = $func($value);
    }

    return $value;
  }

  /**
   * Applies the ucfirst() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processUcfirst($value, array $params): mixed {
    // This function will perform the ucfirst().
    $func = function ($value) {
      $value = (string) $value;
      return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v);
      }
    }
    else {
      $value = $func($value);
    }

    return $value;
  }

  /**
   * Applies the preg_replace() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processPregReplace($value, array $params): mixed {
    // This function will perform the preg_replace().
    $func = function ($value, $pattern, $replacement) {
      return preg_replace($pattern, $replacement, (string) $value);
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v, $params['p1'], $params['p2']);
      }
    }
    else {
      $value = $func($value, $params['p1'], $params['p2']);
    }

    return $value;
  }

  /**
   * Applies the strtotime() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return int|array
   *   The processed value (or array of values).
   */
  private function processStrtotime($value, array $params): mixed {
    // This function will perform the strtotime().
    $func = function ($value) {
      return $value ? strtotime((string) $value) : '';
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v);
      }
    }
    else {
      $value = $func($value);
    }

    return $value;
  }

  /**
   * Applies the date() function to the value.
   *
   * @param int|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processDate($value, array $params): mixed {
    // This function will perform the date().
    $func = function ($value, $format) {
      return $value ? date($format, (int) $value) : '';
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v, $params['p1']);
      }
    }
    else {
      $value = $func($value, $params['p1']);
    }

    return $value;
  }

  /**
   * Applies the trim() function to the value.
   *
   * @param string|array $value
   *   A value to process (or array of values).
   * @param array $params
   *   Function params.
   *
   * @return string|array
   *   The processed value (or array of values).
   */
  private function processTrim($value, array $params): mixed {
    // This function will perform the trim().
    $func = function ($value, $characters) {
      $value = (string) $value;
      return $characters ? trim($value, $characters) : trim($value);
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($v, $params['p1'] ?? '');
      }
    }
    else {
      $value = $func($value, $params['p1'] ?? '');
    }

    return $value;
  }

  /**
   * Searches for entity by its field.
   *
   * @param string|array $value
   *   A field value to search by (or array of values).
   * @param array $params
   *   Lookup params.
   *
   * @return string|null|array
   *   The entity id (or array of ids).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Invalid plugin definition exception.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Plugin not found exception.
   */
  private function processEntityLookup($value, array $params): mixed {
    $storage = $this->entityTypeManager->getStorage($params['p1']);
    $field = ($params['p2'] ?? '') ?: 'label';

    // This function will perform the entity lookup.
    $func = function ($field, $value, $storage) {
      if (!$value) {
        return NULL;
      }

      $entities = $storage->loadByProperties([$field => $value]);
      $entity = reset($entities);
      return $entity ? (string) $entity->id() : NULL;
    };

    if (is_array($value)) {
      foreach ($value as &$v) {
        $v = $func($field, $v, $storage);
      }
    }
    else {
      $value = $func($field, $value, $storage);
    }

    return $value;
  }

  /**
   * Makes a method name out of a string.
   *
   * @param string $s
   *   The function name (like 'preg_replace').
   *
   * @return string
   *   The method name (like 'PregReplace').
   */
  private function makeMethodName(string $s): string {
    $name = preg_replace('![_-]+!', ' ', trim($s));
    $name = preg_replace('!\s+!', ' ', trim($name));
    $name = ucwords($name);
    return str_replace(' ', '', $name);
  }

}
