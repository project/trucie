<?php declare(strict_types=1);

namespace Drupal\trucie;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a trucie importer config.
 */
interface TrucieImporterInterface extends ConfigEntityInterface {

  /**
   * Returns the import entity type id.
   *
   * @return string
   *   The entity type id.
   */
  public function getImportEntityType(): string;

  /**
   * Returns the import entity bundle.
   *
   * @return string
   *   The entity bundle name.
   */
  public function getImportEntityBundle(): string;

  /**
   * Returns file fid.
   *
   * @return string
   *   The uploaded file fid.
   */
  public function getFid(): string;

  /**
   * Sets new source file fid.
   *
   * @param string $fid
   *   New file fid.
   *
   * @return $this
   */
  public function setFid(string $fid): self;

  /**
   * Returns import mode.
   *
   * @return string
   *   The import mode machine name.
   */
  public function getMode(): string;

  /**
   * Returns csv params.
   *
   * @return array
   *   The csv params.
   */
  public function getCsvParams(): array;

  /**
   * Sets the csv params.
   *
   * @param array $params
   *   New csv params.
   *
   * @return $this
   */
  public function setCsvParams(array $params): self;

  /**
   * Returns processors.
   *
   * @return array
   *   The field processors.
   */
  public function getProcessors(): array;

  /**
   * Sets the processors.
   *
   * @param array $processors
   *   New field processors.
   *
   * @return $this
   */
  public function setProcessors(array $processors): self;

  /**
   * Returns delimiter.
   *
   * @return string
   *   The csv field delimiter.
   */
  public function getCsvDelimiter(): string;

  /**
   * Returns enclosure.
   *
   * @return string
   *   The csv string enclosure.
   */
  public function getCsvEnclosure(): string;

  /**
   * Returns default csv enclosure.
   *
   * @return string
   *   The csv string enclosure.
   */
  public function getDefaultCsvEnclosure(): string;

  /**
   * Shows if import logging is enabled.
   *
   * @return bool
   *   TRUE if logging is enabled, otherwise FALSE.
   */
  public function isLoggingEnabled(): bool;

  /**
   * Returns importer params.
   *
   * @return array
   *   Params that can be passed to actual importer.
   */
  public function getParams(): array;

}
